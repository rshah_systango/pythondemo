def select_fruits():
  input_fruit=str(raw_input('Enter the fruit you wish to see the price:-'))
  input_fruit=input_fruit.lower()
  fruits = {
    'orange': 40,
    'apple': 200,
    'banana': 30,
    'grapes': 60,
    'guvava': 35
  }

  if fruits.get(input_fruit):
    print "%s has price %d" %(input_fruit,fruits[input_fruit])
  else:
    print "%s is not available" %(input_fruit) 

select_fruits()    
